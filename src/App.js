import React, {useState} from 'react';
import './App.css';
import Button from './components/Button';
import TodoList from './components/TodoList';


function App() {

  const [todoList, setTodoList]  = useState([]);
  const [singleTodo, setSingleTodo]  = useState('');


        const handleChangeSingleTodo = e => {
            setSingleTodo (e.target.value);
            console.log(singleTodo);
        };


        const buttonClicked = ()=> {
          //alert ('Todo added')
          //get todo list from the state , and store in a variable
          const temp = todoList
          //push the new todo
          temp.push({todo: singleTodo, completed: false})
          //put back the todos array in the state
          setTodoList([...temp])
          //empty the input
          setSingleTodo('')
        };

        const doneButtonClicked = (i) => {
          // onClick will change style to have a red line through the text if completed = true
          //To do this we need to make it into an object with a property called completed
          // with an initial value of false that we can change to true when it is clicked
          // and then back to false if it is clicked again. Basically toggle completed
          const temp = todoList
          temp[i].completed = !temp[i].completed
          // Then return it back to state
          setTodoList([...temp])
          
        };


        const deleteButtonClicked = (i) => {
          //get todo list from the state , and store in a variable
          const temp = todoList
          // find the idx of the todo you want to delete
          //delete the todo
          temp.splice (i,1);

          // return the TodoList array to the state
          setTodoList([...temp])
          
        };



   return (
    <div className="App">
      <header className="App-header">

      </header>

      <section className = 'main'>
        <h1 className = 'title'>To Do List</h1>
      
          <div>
            <form
                onSubmit= {event => {
                  event.preventDefault();
                }}
                >
                <input value={singleTodo} onChange = {handleChangeSingleTodo}/>
                <Button add = {buttonClicked}/>
            </form>
            <TodoList list = {todoList} 
                      deleteButtonClicked={deleteButtonClicked}
                      doneButtonClicked={doneButtonClicked}/>
          </div>

      
      </section>

     
    </div>
  );
}

export default App;
