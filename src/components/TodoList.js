import React, {useState} from 'react';



const TodoList = (props) => {
    console.log(props)
   
    let {list} = props

    


        return (
          <div className = 'myTodos'>
            <h3>My Todos:</h3>
            {
                <ul>  
                    {list.map ((ele,i)=> <li key ={i} className={ele.completed ? 'done' :'doing'}>
                        <button className = 'doneButton' onClick = {()=> props.doneButtonClicked(i)}>Done</button>
                            
                                {ele.todo}
        
                        <button className = 'deleteButton' onClick = {()=> props.deleteButtonClicked(i)}> X </button>
                        </li> )}
                </ul> 
            }
           
          </div>
        );
}    

export default TodoList;
